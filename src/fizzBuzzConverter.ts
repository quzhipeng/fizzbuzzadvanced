export type FizzBuzzType = number | string;

const FIZZ_STR = 'Fizz';
const BUZZ_STR = 'Buzz';
const WHIZZ_STR = 'Whizz';

// tslint:disable-next-line:variable-name
function isDivByNumber(number: number,divNumber:number) {
    return number % divNumber === 0;
}

function includeNumber(num:number,matchNum:string) {
    return String(num).indexOf(matchNum);
}

function isIncludeThreeAndExclusiveFive(number: number) {
    return includeNumber(number, '3') > -1 && includeNumber(number, '5') === -1;
}

function isDivByThreeAndExclusiveFiveOrIncludeSeven(number: number) {
    return isDivByNumber(number, 3) && (includeNumber(number, '7') > -1 || includeNumber(number, '5') === -1);
}

function isDivByFiveAndExclusiveSeven(number: number) {
    return isDivByNumber(number, 5) && includeNumber(number, '7') === -1;
}

// tslint:disable-next-line:variable-name
const fizzBuzzConverter = (number: number): FizzBuzzType => {
    let str = ''
    if(isIncludeThreeAndExclusiveFive(number)){
        str = FIZZ_STR
    }else {
        if (isDivByThreeAndExclusiveFiveOrIncludeSeven(number)) {
            str += FIZZ_STR
        }
        if(isDivByFiveAndExclusiveSeven(number)){
            str += BUZZ_STR
        }
        if(isDivByNumber(number,7)){
            str += WHIZZ_STR
        }
    }
    if(!str){
        str = String(number)
    }
    return str;
}

export default fizzBuzzConverter;
